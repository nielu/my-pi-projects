#!/usr/bin/env python

#
# Example using Dynamic Payloads
# 
#  This is an example of how to use payloads of a varying (dynamic) size.
# 

import time
from RF24 import *
from random import randint


def sendMessage(type, arg1, arg2, arg3, rad):
    msg = str(type) + ':' + str(arg1) + ':' + str(arg2) + ':' + str(arg3)
    print msg + '\n'
    if (rad.write(msg)):
        if (not rad.available()):
            return True
    return False
    
def padaka(rad):
    while (1):
        r = randint(0,255)
        g = randint(0,255)
        b = randint(0,255)
        time.sleep(0.01)
        sendMessage(1, r,g,b, rad)
    
    
def ambi_random(rad):
    r_current = 0
    b_current = 0
    g_current = 0
    while (1):
        sendMessage(1, r_current, g_current, b_current, rad)
        print 'r: ' + str(r_current) + ' g: ' + str(g_current) + ' b: ' + str(b_current) + '\n'
        next = True
        while (next == True ):#need to swap values r with r?current
            r = randint(0, 255)
            b = randint(0, 255)
            g = randint(0, 255)
            temp = r
            r = r_current
            r_current = temp
            temp = b
            b = b_current
            b_current = temp
            temp = g
            g = g_current
            g_current = temp
            dR = r - r_current
            dB = b - b_current
            dG = g - g_current
            while (dR != 0 or dB != 0 or dG != 0):
                #print 'dR: ' +str(dR) + ' dB: '+ str(dB) + ' dG: ' + str(dG) + '\n'  
                r = r + biggerThanZero(dR)
                b = b + biggerThanZero(dB)
                g = g + biggerThanZero(dG)
                sendMessage(1, r, g, b, rad)
                dR = r - r_current
                dB = b - b_current
                dG = g - g_current
                time.sleep(0.05)
            #print 'Next random R ' + str(r) + 'G ' + str(g) + 'B ' + str(b) + '\n'
            next = False
            r_current = r
            b_current = b
            g_current = g
        

def biggerThanZero(d):
    if d > 0:
        return -1
    if d < 0:
        return 1
    return 0

radio = RF24(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ)

pipes = [0xF0F0F0F0E1, 0xF0F0F0F0D2]
min_payload_size = 4
max_payload_size = 32
payload_size_increments_by = 1
next_payload_size = min_payload_size
inp_role = 'none'
send_payload = ''
millis = lambda: int(round(time.time() * 1000))

radio.begin()
radio.enableAckPayload()
radio.enableDynamicPayloads()
radio.setRetries(5,15)
radio.printDetails()

print 'Role: Ping Out, starting transmission'
radio.openWritingPipe(pipes[0])
radio.openReadingPipe(1,pipes[1])
on = 0;
# forever loop
while 1:
    r = 0
    g = 0
    b = 0
    #i = raw_input("What to do?\n0->off\n1->put R G B on\n2->put RGB smoothly\n4->change delay values\n5 - random FTW!\n")
    #i = int(i)
    i = 6
    if (i == 0):
        print 'switching lamp off'
    elif (i == 1 or i == 2):
        r = raw_input('red->')
        g = raw_input('green->')
        b = raw_input('blue->')
        print 'setting values'
    elif(i == 4):
        r = raw_input('transition delay ->')
        g = raw_input('wait delay ->')
        b = 0
    elif(i == 5):
        print 'initating random input'
        ambi_random(radio)
    elif(i == 6):
        print 'happpy padaczka'
        padaka(radio)
    else:
        print 'lol wut m8'
        continue
    if (sendMessage(i,r,g,b,radio)):
        print 'message sent'
    else:
        print 'something went wrong'


'''
#message structure:
#function:arg1:arg2:arg3
#functions
0 - turn off
1 - set 3 colors now arg1 : R, arg2: G, arg3: B
2 - set smooth transition arg1, arg2, arg3
3 - set vector arg1 v1: arg2 v2
4 - set timing : arg1 transition_delay arg2 wait_delay
...
255 - error 
'''
