
    #Simple socket server using threads

 
import socket
import sys
#import cv2
import numpy
import threading
import time 
import pigpio
 
HOST = ''   # Symbolic name, meaning all available interfaces
PORT = 8777 # Arbitrary non-privileged port
CAM_PORT = PORT + 1
EXIT_FLAG = 0


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cam_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'
 
#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
    cam_s.bind((HOST, CAM_PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'
 
#Start listening on socket
s.listen(1)
cam_s.listen(1)
print 'Socket now listening'

def move():
    conn, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
    lastMove = "STOP"
    while 1:
        #keep this connection
        data = conn.recv(4)
        if not data or EXIT_FLAG == 1:
            s.close();
            print "Exit"
            break
        bytes = bytearray(data)
        move = bytes[1]
        pwr = bytes[2]
       
        jazda(move, pwr)
    s.close()


def watch():
    conn, addr = cam_s.accept()
    print 'Cam connected to ' + addr[0]
    data = conn.recv(4)
    if not data:
        s.close()
        print 'Cam exit'
        return
    bytes = bytearray(data)
    fps = bytes[1]
    wait = float(6/(fps*10))
    print 'Running at ' + str(fps) + ' fps, equals ' + str(wait) + ' wait time'
    while 1:
        if (EXIT_FLAG == 1):
            s.close()
            print 'Cam exit'
            return
        '''capture = cv2.VideoCapture(0)
        ret, frame = capture.read()

        encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
        result, imgencode = cv2.imencode('.jpg', frame, encode_param)
        data = numpy.array(imgencode)
        stringData = data.tostring()
        
        conn.send(str(len(stringData)).ljust(16))'''
        time.sleep(wait)
        
def jazda_stop():
    gpio = pigpio.pi()
    gpio.write(4, 0)
    gpio.write(17, 0)
    gpio.write(22, 0)
    gpio.write(26, 0)
    gpio.write(20, 0)    
    gpio.write(23, 0)

def jazda(kierunek, moc):
    if kierunek == 0 or kierunek == 1 or kierunek == 2:
        jazda_stop()
        return
    gpio = pigpio.pi()
    
    
    if kierunek == 4: # PRZOD
        gpio.set_PWM_dutycycle(4, moc)
        gpio.set_PWM_dutycycle(17, moc)
        gpio.write(22, 1)
        gpio.write(26, 1)
    elif kierunek == 8: # TYL
        gpio.set_PWM_dutycycle(4, moc)
        gpio.set_PWM_dutycycle(17, moc)
        gpio.write(20, 1)
        gpio.write(23, 1)
    elif kierunek == 5 or kierunek == 9: #LP
        gpio.set_PWM_dutycycle(17, moc)
        gpio.write(22, 1)
    elif kierunek == 6 or kierunek == 10: #PP
        gpio.set_PWM_dutycycle(4, moc)
        gpio.write(26, 1)
    
    
       

camThread = threading.Thread(target = watch)
moveThread = threading.Thread(target = move)
camThread.start()
moveThread.start()




while 1:
    time.sleep(2)
    if camThread.isAlive() == False or moveThread.isAlive() == False:
        EXIT_FLAG = 1
        jazda_stop()
        print 'killing threads'
        break
print "Exit"