from flask import Flask, render_template, request
import connection as ambi

radio = ambi.Radio()
app = Flask(__name__)

ip_adress = '0.0.0.0'
port = 8081

# Create a dictionary called pins to store the pin number, name, and pin state:
colors = {
   'red' :  0,
   'green' :  0,
   'blue' :  0
   }

def update():
    radio.setColor(colors['red'], colors['green'], colors['blue'])


@app.route("/")
def main():
   # Put the pin dictionary into the template data dictionary:
   templateData = {
      'colors' : colors
      }
   # Pass the template data into the template main.html and return it to the user
   return render_template('main.html', **templateData)
@app.route("/<color>/<value>")
def action(color, value):
    colors[color] = int(value)
            # Along with the pin dictionary, put the message into the template data dictionary:
    templateData = {
      'message' : 'OK',
      'colors' : colors
      }
    update()
    return render_template('main.html', **templateData)

#   return render_template('main.html', **templateData)

# The function below is executed when someone requests a URL with the pin number and action in it:
#@app.route("/<changePin>/<action>")
#def action(changePin, action):
#   # Convert the pin from the URL into an integer:
#   changePin = int(changePin)
#   # Get the device name for the pin being changed:
#   deviceName = pins[changePin]['name']
#   # If the action part of the URL is "on," execute the code indented below:
 #  if action == "on":
#      # Set the pin high:
#      GPIO.output(changePin, GPIO.HIGH)
#      # Save the status message to be passed into the template:
#      message = "Turned " + deviceName + " on."
#   if action == "off":
#      GPIO.output(changePin, GPIO.LOW)
#      message = "Turned " + deviceName + " off."
#   if action == "toggle":
#      # Read the pin and set it to whatever it isn't (that is, toggle it):
#      GPIO.output(changePin, not GPIO.input(changePin))
#      message = "Toggled " + deviceName + "."

   # For each pin, read the pin state and store it in the pins dictionary:
#   for pin in pins:
#      pins[pin]['state'] = GPIO.input(pin)

   # Along with the pin dictionary, put the message into the template data dictionary:
#   templateData = {
#      'message' : message,
#      'pins' : pins
#   }

#   return render_template('main.html', **templateData)

if __name__ == "__main__":
   app.run(host=ip_adress, port=port, debug=True)
   #radio.init()
   update()