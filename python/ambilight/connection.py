import time
from RF24 import *
from random import randint


class Radio:
    #radio = RF24(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ)

    def __init__(self):
        self.radio = RF24(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ)
        pipes = [0xF0F0F0F0E1, 0xF0F0F0F0D2]
        self.radio.begin()
        self.radio.enableAckPayload()
        self.radio.enableDynamicPayloads()
        self.radio.setRetries(5,15)
        self.radio.printDetails()
    
        print 'Role: Ping Out, starting transmission'
        self.radio.openWritingPipe(pipes[0])
        self.radio.openReadingPipe(1,pipes[1])
    
    
    
    def sendMessage(self, type, arg1, arg2, arg3):
        msg = str(type) + ':' + str(arg1) + ':' + str(arg2) + ':' + str(arg3)
        print msg + '\n'
        if (self.radio.write(msg)):
            if (not self.radio.available()):
                return True
        return False
    
    def setColor(self, red,green,blue):
        return self.sendMessage(1, red, green, blue)


    if __name__ == "__main__":
        print 'This is a module!'
        

