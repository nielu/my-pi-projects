#!/usr/bin/env python

#
# Example using Dynamic Payloads
# 
#  This is an example of how to use payloads of a varying (dynamic) size.
# 

import time
import sys
from RF24 import *
from random import randint


def sendMessage(arg1, arg2, arg3, rad):
    msg = '1' + ':' + arg1 + ':' + arg2 + ':' + arg3
    if (rad.write(msg)):
        if (not rad.available()):
            return True
    return False
    
if (len(sys.argv) != 4):
    print 'Usage ambi_console.py red green blue'
    sys.exit()

red = sys.argv[1]
green = sys.argv[2]
blue = sys.argv[3]

radio = RF24(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ)

pipes = [0xF0F0F0F0E1, 0xF0F0F0F0D2]
min_payload_size = 4
max_payload_size = 32
payload_size_increments_by = 1
next_payload_size = min_payload_size
inp_role = 'none'
send_payload = ''
millis = lambda: int(round(time.time() * 1000))

radio.begin()
radio.enableAckPayload()
radio.enableDynamicPayloads()
radio.setRetries(5,15)

radio.openWritingPipe(pipes[0])
radio.openReadingPipe(1,pipes[1])

sendMessage(red,green,blue, radio)
