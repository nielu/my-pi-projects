#!/usr/bin/env python

#
# Example using Dynamic Payloads
# 
#  This is an example of how to use payloads of a varying (dynamic) size.
# 

import time
import sys
from RF24 import *
from random import randint
import MySQLdb as db


def sendMessage(arg1, arg2, arg3, rad):
    msg = '1' + ':' + arg1 + ':' + arg2 + ':' + arg3
    if (rad.write(msg)):
        if (not rad.available()):
            return True
    return False
    
